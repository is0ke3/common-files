set tabstop=2
set expandtab
set autoindent
set tw=60
set relativenumber
set shiftwidth=2
set hlsearch
set undolevels=1000
set noswapfile
set nowrap

set undodir=~/.vi/undo

hi Visual cterm=NONE ctermfg=51 ctermbg=237
"nmap u :earlier<CR>
